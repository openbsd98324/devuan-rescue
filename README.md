# devuan-rescue

## Content 

Preinstalled rootfs, with modules, firmware and kernel for ascii.

devuan-rescue rootfs with firmware and devuan rescue with a little desktop

## Arch
devuan for AMD64

# Desktop

![](media/1630751392-fltk-flde.png)


"https://gitlab.com/openbsd98324/devuan-rescue/-/raw/main/ascii-desktop/amd64/DEVUAN-RESCUE-X11-DESKTOP.tar.gz"


# Grub

/grub.cfg was added.

It contains a live slack into the root directory for managing disks or installing slackware.

/root/EFI/BOOT   <-- with slackware live (to ram, with initramfs)

